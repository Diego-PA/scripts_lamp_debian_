#!/usr/bin/bash

DOMINIO=$1
SUBDOMINIO=$2

cp vhost_debian.conf /etc/apache2/sites-available/"${SUBDOMINIO}".conf

sed -i "s/servidor/${HOSTNAME}/g" /etc/apache2/sites-available/"${SUBDOMINIO}".conf

sed -i "s/PROYECTOS/\/var\/www\/html\/proyectos\/${SUBDOMINIO}\/wordpress/g" /etc/apache2/sites-available/"${SUBDOMINIO}".conf

sed -i "s/DOMINIO/${DOMINIO}/g" /etc/apache2/sites-available/"${SUBDOMINIO}".conf

a2ensite "${SUBDOMINIO}".conf

sed -i "3i\127.0.1.1 \t${DOMINIO}" /etc/hosts

systemctl reload apache2
