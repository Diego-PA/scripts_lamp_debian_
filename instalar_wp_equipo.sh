#!/bin/bash

OPS=0

until [ "${OPS}" -eq 2 ] || [ "${OPS}" -eq 1 ]; do
	
	HORA=$(date +%R | cut -d ":" -f 1)

	if [ ${HORA} -gt 5 ] && [ ${HORA} -lt 12 ]; then

		echo -n "Buenos dias"

	elif [ ${HORA} -gt 11 ] && [ ${HORA} -lt 19 ]; then

		echo -n "Buenas tardes"

	else

		echo -n "Buenas noches"

	fi

	echo -e ", la instalacion se realizara en:"
	echo -e "1) Servidor local"
	echo -e "2) Servidor remoto"
	echo -n "Seleccione una opcion: "

	read OPS

done

if [ ! -f dominios.txt ]; then

	touch dominios.txt

fi

if [ "${OPS}" -eq 1 ]; then

	./instalar_wp.sh

else

	./instalar_wp_remoto.sh

fi
