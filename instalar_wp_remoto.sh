#!/bin/bash

echo -e "\nInstalacion en servidor remoto"

echo -ne "\nIngrese la direccion del servidor remoto: "
read USUARIO_REMOTO

echo -ne "\nIngrese la contraseña del usuario root de mariadb del servidor remoto: "
read -s PASSWORD

IP_REMOTA=$(echo "${USUARIO_REMOTO}" | cut --complement -d "@" -f 1)
USUARIO_REMOTO=$(echo "${USUARIO_REMOTO}" | cut -d "@" -f 1)

echo -ne "\nIngrese el nombre del dominio: "
read DOMINIO

VALIDAR=$(grep "${DOMINIO}"$ dominios.txt)
until [ $? -eq 1 ]; do

	echo -e ""
	echo -e "El dominio ${DOMINIO} ya existe"
	echo -e ""
	echo -n "Ingrese un dominio distinto: "
	read DOMINIO
	VALIDAR=$(grep "${DOMINIO}"$ dominios.txt)

done

echo "${DOMINIO}" >> dominios.txt

echo -e ""
echo -n "Ingrese el nombre de la nueva base de datos: "
read BDNAME

ssh "${USUARIO_REMOTO}"@"${IP_REMOTA}" "mysql -u root -e 'SHOW DATABASES' -p'${PASSWORD}'" > bases_remotas.txt
VALIDAR=$(grep "${BDNAME}"$ bases.txt)

until [ $? -eq 1 ]; do

	echo -e ""
	echo "Ya existe una BD con nombre ${BDNAME}"
	echo -e ""
	echo -n "Elija un nombre distinto para las BD: "
	read BDNAME
	VALIDAR=$(grep "${BDNAME}"$ bases.txt)

done


echo -e ""
echo -n "Ingrese el nombre de usuario: "
read BDUSER

ssh "${USUARIO_REMOTO}"@"${IP_REMOTA}" "mysql -u root --database mysql -e'SELECT user from user' -p'${PASSWORD}'" > usuarios_remotas.txt
VALIDAR=$(grep "${BDUSER}"$ usuarios.txt)

until [ $? -eq 1 ]; do

	echo -e ""
	echo -e "Ya existe el nombre de usuario ${BDUSER}"
	echo -e ""
	echo -n "Elija un nombre de usuario distinto: "
	read BDUSER
	VALIDAR=$(grep "${BDUSER}"$ usuarios.txt)

done

echo -e ""
echo -n "Ingrese la contraseña para el nuevo usuario: "
read USERPASS

SUBDOMINIO="$(echo ${DOMINIO} | cut -d "." -f 1)"
RUTA="/var/www/html/proyectos/${SUBDOMINIO}"

until [ ! -d ${RUTA} ]; do
	
	echo -e ""
	echo -e "El ya existe el proyecto ${SUBDOMINIO}"
	echo -e ""
	echo -n "Elija un nombre de proyecto distinto: "
	read SUBDOMINIO
	RUTA="/var/www/html/proyectos/${SUBDOMINIO}"

done

mkdir "${RUTA}"

./creaBD_remoto.sh ${BDNAME} ${BDUSER} ${USERPASS} ${USUARIO_REMOTO} ${IP_REMOTA} ${PASSWORD}

SO="/etc/redhat-release"
if [ -f ${SO} ]; then

	./crea_vHost_redhat.sh ${DOMINIO} ${SUBDOMINIO}

else
	./vHost_debian.sh ${DOMINIO} ${SUBDOMINIO}

fi

PROYECTOS="/var/www/html/proyectos/${SUBDOMINIO}"

cd /tmp
wget -nc https://wordpress.org/latest.tar.gz
tar -zxf latest.tar.gz -C "${PROYECTOS}"

PROYECTO="${PROYECTOS}/wordpress"

cp "${PROYECTO}"/wp-config-sample.php "${PROYECTO}"/wp-config.php

sed -i "s/database_name_here/${BDNAME}/g" "${PROYECTO}"/wp-config.php
sed -i "s/username_here/${BDUSER}/g" "${PROYECTO}"/wp-config.php
sed -i "s/password_here/${USERPASS}/g" "${PROYECTO}"/wp-config.php
sed -i "s/localhost/${IP_REMOTA}/g" "${PROYECTO}"/wp-config.php
