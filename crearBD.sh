#!/usr/bin/bash

BDNAME=$1
BDUSER=$2
USERPASS=$3
PASSWORD=$4

mysql -u root -e "CREATE DATABASE ${BDNAME}" -p"${PASSWORD}"
mysql -u root -e "CREATE USER '${BDUSER}'@'localhost' IDENTIFIED BY '${USERPASS}'" -p"${PASSWORD}"
mysql -u root -e "GRANT ALL PRIVILEGES ON ${BDNAME}.* TO '${BDUSER}'@'localhost'" -p"${PASSWORD}"
#mysql -u root -e 'SHOW DATABASES' -p"${PASSWORD}"
#mysql -u root --database mysql -e 'SELECT user FROM user' -p"${PASSWORD}"

